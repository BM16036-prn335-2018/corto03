#include "stdio.h"
#include "stdlib.h"
#include "string.h" //contiene la definición de macros, constantes, funciones y tipos y algunas operaciones de manipulación de memoria, es necesario para obtener el tamano de una cadena 


struct nodo	// Esta estructura nos permitira ejecutar el concepto de pilas es la base por la que se ejecutan las pilas	
{
	int info;				
	struct nodo *sig;
};

struct nodo *raiz=NULL; // la structura nod raiz sera tomada como la cima de la pila en este caso esta apuntado a NULL para indicar que la pila esta vacia 

void insetar (int x) // funcion que nos permitira insertar un nuevo elemnto (nodo)  a la pila
{
	struct nodo *nuevo;	//se crea una structura que se encarga de los elementod nuevos
	nuevo = malloc(sizeof(struct nodo)); //asignacion del bloque de memoria a nuevo

	nuevo->info = x; // asignamos X a la variable info por medio de la estructura nodo nuevo  

	if (raiz == NULL)	// se comprueva si la pila esta vacia para la asignacion de las variables
	{
		raiz = nuevo; // raiz toma los elementos que contiene la estructura nuevo
		nuevo->sig = NULL; //el puntero sig que esta dentro de la estructura nodo nuevo apunta a NULL
	}
	else 				// en el caso de que haya un elemnto en la pila es decir que la pila no este vacia
	{
		nuevo->sig = raiz; // el puntero sig que esta dentro de la estructura nod sig apuntara a raiz que hasta el momento era la cima
		raiz = nuevo; //raiz tomara los elemntos de nuevo 
	}
}

void imprimir() //la funcion imprimir muestra en pantalla la lista de todos los elemntos de la pila
{
	struct nodo *reco=raiz; //se crea una structura nodo reco que apunta a raiz (cima)
	printf("Lista completa.\n");
	while (reco!=NULL) //se comprueva que reco apunte a algo diferente de NULL
	{
		printf("%i\n",reco->info); // se imprime la variable info que esta contenida en la estructura nodo reco
		reco=reco->sig;	//reco tomara los elemtos que estan en el nodo que estan en el puntero sig 
	}
	printf("\n");
}

void imprimirNumero() //esta funcion hace lo mismo que imprimir() solo con una diferencia y es que imprime los elementos horizontalmente sin saltos de linea
{
	struct nodo *num=raiz;
	while (num!=NULL)
	{
		printf("%i",num->info);
		num=num->sig;	
	}
	printf("\n");
}

int contar() // esta funcion cuenta el numero de nodos que hay en la pila devolviendo un numero 
{
	struct nodo *cont=raiz; // se crea una structura nodo cont que apunta a raiz (cima)
	int contador=0; 
	while(cont!=NULL)//se comprueva que cont apunte a algo diferente de NULL
	{
		contador++; // se incrementa en uno la variable contador
		cont=cont->sig; //cont tomara los elemtos que estan en el nodo que estan en el puntero sig 
	}
	return contador;
}

int estaVacia() // esta funcion comprueva si la pila es vacia o no
{
	struct nodo *vacio=raiz; // se crea una structura nodo vacio que apunta a raiz (cima)

	if (vacio != NULL) //se comprueva que vacio apunte a algo diferente de NULL si es asi devuelve 0
	{
		return 0;
	}
	else // de ser contrario devuelve 1
	{
		return 1;
	}
}

void Reemplazar(int nuevo, int viejo) // esta funcion reempla un elemnto de la pila por otro 
{
	struct nodo *reemp=raiz; // se crea una structura nodo reemp que apunta a raiz (cima)

	while(reemp!=NULL) //se comprueva que reemp apunte a algo diferente de NULL 
	{
			if (reemp->info==viejo) // se comprueba si la variable info que esta en la estructura nodo reemp es igual a viejo que es el numero que deceamos reemplazar
		{
			reemp->info=nuevo; //si es asi la variable info que esta en la estructura nodo reemp se reemplazara por nuevo 
		}
		reemp=reemp->sig; //reemp tomara los elemtos que estan en el nodo que estan en el puntero sig 
	}
}

int extraer() // esta funcion permite extraer el elemnto que esta en la cima de la pila y devuelve el dato que fue eliminado
{
	if (raiz != NULL) //se comprueva que raiz apunte a algo diferente de NULL 
	{
		int informacion = raiz->info; //la variable informacion unicamente toma el valor info que esta dentro de raiz
		struct nodo *bor = raiz; // se crea una structura nodo bor que apunta a raiz (cima)
		raiz = raiz->sig; // raiz toma los elemtos del nodo que estan en el puntero raiz siguiente
		free(bor); // se libera el espacion de memoria de bor
		return informacion; 
	}
	else
	{
		return -1;
	}
}

void liberar()
{
	struct nodo *reco = raiz; // se crea una structura nodo reco que apunta a raiz (cima)
	struct nodo *bor; // se crea una structura nodo bor
	while (reco != NULL) //se comprueva que reco apunte a algo diferente de NULL 
	{
		bor = reco; //bor toma los elemtos de reco 
		reco = reco->sig;//reco toma los elemntos del nodo siguiente (nodo que esta debajo)
		free(bor);//se libera el espacio de memoria de bor 
	}
}

int main()
{

	int nuevo,viejo;
	char numero[100];

	insetar(10); // se invoca a la funcion inserta pasandole una variable (10)
	insetar(40); // se invoca a la funcion inserta pasandole una variable (40)
	insetar(3); // se invoca a la funcion inserta pasandole una variable (3)
	printf("=============================================\n");
	imprimir();// se invoca a la funcion Imprimir que muestra en pantalla los datos de la pila
	printf("=============================================\n");
	printf("Pila Vacia: %s\n",estaVacia()?"true":"false"); // muestra en pantalla el resultado llamando a la funcion estaVacia() si este devuelve un 0 imprime "true" y si devuelve un 1 imprime un "false"
	printf("Numero de nodos %d\n",contar()); // muetra en pantalla el resultado de la funcion contar 
	printf("Extraemos de la pila:%i\n",extraer()); // muestra en pantalla el resultado de la funcion extraer que libera e espacio de memoria de la cima y devuelve el dato que contenia 
	printf("Numero de nodos %d\n",contar());// muetra en pantalla el resultado de la funcion contar
	printf("Extraemos de la pila:%i\n",extraer());// muestra en pantalla el resultado de la funcion extraer que libera e espacio de memoria de la cima y devuelve el dato que contenia 
	printf("Numero de nodos %d\n",contar());// muetra en pantalla el resultado de la funcion contar
	printf("Extraemos de la pila:%i\n",extraer());// muestra en pantalla el resultado de la funcion extraer que libera e espacio de memoria de la cima y devuelve el dato que contenia 
	printf("Numero de nodos %d\n",contar());// muetra en pantalla el resultado de la funcion contar
	printf("Pila Vacia: %s\n",estaVacia()?"true":"false"); // muestra en pantalla el resultado llamando a la funcion estaVacia() si este devuelve un 0 imprime "true" y si devuelve un 1 imprime un "false"
	printf("=============================================\n");
	liberar(); // se invoca la funcion liberar que libera el espacio de memoria de todos los nodos de la pila
	printf("Pulse enter para continuar\n");
	getchar(); // se imcorpora una pausa 
	system("clear");// limpia la pantalla por medio de comandos del sistema operativo NOTA: POR CUALQUIER ERROR SUSTITUIR system("cls"); POR system("clear"); o viceversa devido a que System hace uso de comandos del mismo sistema operativo
	
	printf("=============================================\n");
	printf("Ingrse un numero para luego invertir el sentido \n");
	gets(numero); // resive entrada desde teclado
	int tamanio=strlen(numero); // Para obtener el largo de una cadena
	for (int i = 0; i < tamanio; ++i) // el for ayuda a separa cada caracter de la cadena 
	{
		insetar((numero[i]-'0')); //devido a que insetar solo acepta variables de tipo int se tuvo que convertir una variable de tipo char a int
	}
	printf("El numero invertido es: "); 
	imprimirNumero();// se invoca a la funcion Imprimir que muestra en pantalla los datos de la pila sin saltos de linea
	printf("\n=============================================\n");
	printf("Pulse enter para continuar\n");
	getchar(); // se imcorpora una pausa 
	system("clear");// limpia la pantalla por medio de comandos del sistema operativo NOTA: POR CUALQUIER ERROR SUSTITUIR system("cls"); POR system("clear"); o viceversa devido a que System hace uso de comandos del mismo sistema operativo

	printf("=============================================\n");
	imprimir();// se invoca a la funcion Imprimir que muestra en pantalla los datos de la pila
	printf("Para reemplazar un numero ingrese el nuevo numero\n");
	scanf("%d",&nuevo);
	printf("Ingrese el numero que quiere reemplazar\n");
	scanf("%d",&viejo);
	Reemplazar(nuevo,viejo);// se invoca la funcion Reemplazar que manda dos numero nuevo y viejo
	imprimir(); // se invoca a la funcion Imprimir que muestra en pantalla los datos de la pila sin saltos de linea
	printf("=============================================\n");
	liberar();// se invoca la funcion liberar que libera el espacio de memoria de todos los nodos de la pila
	printf("Pulse enter para continuar\n");
	getchar(); // se imcorpora una pausa 
	system("clear");// limpia la pantalla por medio de comandos del sistema operativo NOTA: POR CUALQUIER ERROR SUSTITUIR system("cls"); POR system("clear") <<este es de linux>> ; o viceversa devido a que System hace uso de comandos del mismo sistema operativo
	return 0;
}